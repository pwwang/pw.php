#!/usr/bin/env php
<?php

require_once __DIR__ . "/../vendor/autoload.php";

$arg = pw\Args\Args::factory()->done();

$logger    = new \Monolog\Logger(basename($argv[0]));
$logger    ->pushProcessor (new \Monolog\Processor\PsrLogMessageProcessor());
$rootdir   = getRootDir($srcdir);
pw\Utils\Utils::dump($arg->get());
call ($arg["-"]);

function call ($command) {
	$funcfile = $command;
	if (strpos($command, ":") !== false)
		list ($funcfile, $func) = explode(":", $command);
		
	require_once __DIR__ . "/commands/$funcfile.php";
	$func = isset($func) ? $func : $funcfile;
	
	$keywords = ['list'];
	if (in_array($func, $keywords)) {
		$func = "_$func";
	}
	$func ();
}

function getRootDir (&$srcdir) {
	$isPhar    = strpos (__FILE__, "phar://") === 0;
	$dir       = $isPhar ? dirname(Phar::running(false)) : __DIR__;
	$srcdir    = dirname ($dir);
	$devvdr    = $srcdir . DIRECTORY_SEPARATOR . "vendor";
	return is_dir ($devvdr)
		? dirname ($dir)
		: dirname (dirname (dirname (dirname ($dir))));
}