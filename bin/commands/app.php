<?php
use pw\Fs\Dir;
use pw\Utils\Utils;

function create () {
	global $rootdir, $arg, $logger;
	$appdir = new Dir ("$rootdir/app/" . $arg['path']);
	
	if ($appdir->exists()) {
		echo "App: " . Utils::color ($arg['path']) . " exists, quit.\n";
		exit;
	}
	
	$logger->addInfo ("Creating view folder: " . $appdir->path("__view"));
	if (!is_dir($appdir->path("__view")))
		mkdir ($appdir->path("__view"));
		
	$logger->addInfo ("Creating controller file: " . $appdir->path("controller.php"));
	createController ($appdir->path("controller.php"));
	
	$logger->addInfo ("Creating middleware file: " . $appdir->path("middleware.php"));
	createMiddleware ($appdir->path("middleware.php"));
	
	$logger->addInfo ("Creating model file: " . $appdir->path("model.php"));
	createModel ($appdir->path("model.php"));
	
	$logger->addInfo ("Creating route file: " . $appdir->path("route.php"));
	createModel ($appdir->path("route.php"));
}

function createController () {
	

}