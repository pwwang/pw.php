<?php
use pw\Utils\Utils;
use pw\Php\Core\Core;
function serve () {
    global $arg, $rootdir;
    echo Utils::color("PHP Version: " . PHP_VERSION . ", pw.php Version: " . Core::VERSION . "." . PHP_EOL, "green");
    echo Utils::color("Built-in server started at " . Utils::color("localhost:" . $arg['port'], "yellow") . "\n", "green");
    echo Utils::color("Document root: " . Utils::color($rootdir . DIRECTORY_SEPARATOR, "yellow") . "\n", "green");
    echo Utils::color("Hit CTRL+C to quit.\n", "green") . PHP_EOL;
    system ("php -S localhost:{$arg['port']} -t '$rootdir'");
}