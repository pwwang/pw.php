<?php

use pw\Utils\Utils;
use pw\Fs\File;

require_once __DIR__ . "/static.php";

function install () {
	//$composer = installComposer();
	//installComposerPkgs ($composer);
	makeFolders (['app']);
	copySrc (['index.ph_', 'config.ph_', 'static.ph_']);
    addBowerDirToStatic ();
    update (); // static
    addBowerDirToGitIgnore ();
    writeBowerRC ();
}

function makeFolders($folders) {
	global $logger, $rootdir;
	$logger->addInfo ('Making folders: ', $folders);
	foreach ($folders as $folder) {
		if (!is_dir ("$rootdir/$folder"))
			mkdir ("$rootdir/$folder");
	}
}

/*
function installComposer () {
	global $logger, $rootdir;
	$avail_commands = [
		"composer",
		"composer.phar",
		"php \"$rootdir/composer\"",
		"php \"$rootdir/composer.phar\""
	];
	$instr  = "Validates a composer.json and composer.lock";
	
	foreach ($avail_commands as $ac) {
		$exists = Utils::commandExists ($ac, $instr);
		if ($exists) {
			$logger->addInfo ("Composer exists, skip.");
			return $ac;
		}
	}
	
	$logger->addInfo ("Composer does not exist, installing ...");
	system (
		PHP_BINARY . ' -r "readfile(\'https://getcomposer.org/installer\');" | ' .
		PHP_BINARY . ' -- --install-dir="' . $rootdir . '/bin"'
	);
	return PHP_BINARY . ' "' . $rootdir . '/bin/composer.phar"';
}

function installComposerPkgs ($composer) {
	global $logger;
	$logger->addInfo ("Installing composer packages ...");
	system ($composer . " update");
}
*/
function copySrc ($srcfiles) {
    global $logger, $rootdir, $srcdir;
    foreach ($srcfiles as $srcfile) {
        $logger->addInfo ("Copying $srcfile ...");
        $sf      = new File ("$srcdir/framework/pw/Php/$srcfile");
        $content = str_replace ('{rootdir}', $rootdir, $sf->contents());
        $outfile = new File ("$rootdir/" . str_replace(".ph_", ".php", $srcfile));
        $outfile -> contents ($content);
    }
}

function addBowerDirToStatic () {
    global $arg;
    $rule = "^" . basename($arg["bower-dir"]) . "\/";
    if (!isset($arg["r"]) or !is_array($arg["r"])) $arg["r"] = [$rule];
    else $arg["r"][] = $rule;
    add ();
}

function addBowerDirToGitIgnore () {
    global $logger, $arg, $rootdir;
    $gifile = new File ("$rootdir/.gitignore");
	$igns   = $gifile->exists() ? $gifile->lines() : [];
    $bd     = basename($arg["bower-dir"]);
    
    $logger->addInfo ("Adding bower component dir to .gitignore ...");
    if (!in_array($bd, $igns)) $igns[] = $bd;
    $gifile->contents (implode("\n", $igns) . "\n");
}

function writeBowerRC () {
    global $logger, $arg, $rootdir;
    $bd     = basename($arg["bower-dir"]);
    
    $logger->addInfo ("Writing bower component dir to .bowerrc ...");
    $config = ["directory" => $bd];
    $brcfile= new File("$rootdir/.bowerrc");
    $brcfile->contents (json_encode($config));
}
