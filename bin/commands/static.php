<?php

use pw\Fs\File;
use pw\Utils\Utils;

function _getList () {
	global $rootdir;
	$stafile = new File ("$rootdir/static.php");
	return $stafile->data();
}

function _saveList (&$statics) {
	global $rootdir;
	$stafile = new File ("$rootdir/static.php");
	$stafile -> open ("w");
	$stafile -> puts ("<?php");
	$stafile -> puts ("return [");
	foreach ($statics as $static)
		$stafile->puts ("\t'$static',");
	$stafile -> puts ("];");
	$stafile -> close();
}

function _list () {
	global $argv;
	$statics = _getList();
	
	if (empty($statics)) {
		echo "No static rules yet.\n";
		echo "Use `{$argv[0]} static:add` to add some.\n";
	} else {
		echo "Static URIs:\n";
		foreach ($statics as $static) {
			echo "-  $static\n";
		}
	}
}

function add () {
	global $arg, $logger;
	$statics = $arg["r"];
	
	$existStatics = _getList();
	foreach ($statics as $static) {
		if (in_array($static, $existStatics)) {
			$logger->addInfo ("Rule $static exists, skip.");
		} else {
			$existStatics[] = $static;
			$logger->addInfo ("Rule $static added.");
		}
	}
	_saveList ($existStatics);
}

function update () {
	
	$statics = _getList(); 	
	_updateApache ($statics);
}

function delete () {
	global $argv;
	$statics = _getList();
	
	if (empty($statics)) {
		echo "No static rules yet.\n";
		echo "Use `{$argv[0]} static:add` to add some.\n";
		exit;
	} 
	echo "\nStatic URIs:\n\n";
	foreach ($statics as $i => $static) {
		echo Utils::color("[". ($i+1) . "]", "green") . "  $static\n";
	}
	
	$select = "Select rule # to delete (use CTRL+C to quit): ";
	echo "\n" . $select;
	while (true) {
		$c = fgets(STDIN);
		if (!preg_match("/^\d+$/", $c) or $c < 1 or $c > sizeof($statics)) {
			echo $select;
			continue;
		} else {
			$rm = $statics[$c - 1];
			unset ($statics[$c - 1]);
			_saveList($statics);
			echo Utils::color ("Rule $rm removed.", "red") . "\n";
			delete();
		}
	}
}

function _updateApache (&$statics) {
	global $logger, $rootdir;
	$logger->addInfo ("Updating static rules for .htaccess ...");
	
	$statics = _getList();
	
	$htfile  = new File ("$rootdir/.htaccess");
	if (!empty($statics))
	$htfile  -> contents ("RewriteEngine on\n\nRewriteRule !("
		. implode ("|", $statics)
		. ") index.php [L]");
}
