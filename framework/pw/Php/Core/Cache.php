<?php

namespace pw\Php\Core;

class Cache {
	
	static private $caches = [];
	static private $cache0;
	
	static public function init () {
		$configs = Config::get ("Cache");
		if (isset($configs["driver"]))
			$configs = [$configs];
		
		$i = 0;	
		foreach ($configs as $key => $config) {
			extract ($config);
			switch ($driver) {
				case "MongoDB":
					$manager    = new \MongoDB\Driver\Manager("mongodb://$host:$port");
					$collection = new \MongoDB\Collection($manager, "$db.$table");
					self::$caches[$key] = new \Doctrine\Common\Cache\MongoDBCache ($collection);
					break;
				case "SQLite3":
					$sqlite3    = new \SQLite3 ($dbfile, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE, $encrypt);
					self::$caches[$key] = new \Doctrine\Common\Cache\SQLite3Cache ($collection);
					break;
				case "Memcache":
					$memcache = new \Memcache();
					$memcache->connect($host, $port);
					self::$caches[$key] = new \Doctrine\Common\Cache\MemcacheCache();
					self::$caches[$key]->setMemcache($memcache);
					break;
				case "Memcached":
					$memcached = new \Memcached();
					$memcached->addServer($host, $port);
					
					self::$caches[$key] = new \Doctrine\Common\Cache\MemcachedCache();
					self::$caches[$key]->setMemcached($memcached);
					break;
				case "Redis":
					$redis = new \Redis();
					$redis->connect($host, $port);
					
					self::$caches[$key] = new \Doctrine\Common\Cache\RedisCache();
					self::$caches[$key]->setRedis($redis);
					break;
				case "Filesystem":
					self::$caches[$key] = new \Doctrine\Common\Cache\FilesystemCache ($dir, $ext);
					break;
				case "PhpFile":
					self::$caches[$key] = new \Doctrine\Common\Cache\PhpFileCache ($dir, $ext);
					break;
				default:
					$class = new \ReflectionClass ("\Doctrine\Common\Cache\{$driver}Cache");
					self::$caches[$key] = $class->newInstance();
					break;
			}
			$i++ == 0 and self::$cache0 = self::$caches[$key];
		}
		
		
	}
	
	static public function __callStatic ($name, $arguments) {
		return call_user_func_array ([self::$cache0, $name], $arguments);
	}
	
	static public function get ($key = null) {
		if (!is_null($key))
			return self::$caches[$key];
		return self::$cache0;
	}

}