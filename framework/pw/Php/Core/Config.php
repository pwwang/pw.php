<?php
namespace pw\Php\Core;

class Config {
	
	static private $config;
	
	static public function init () {
		self::$config = \Noodlehaus\Config::load (Path::CONFIG_FILE);
	}
	
	static public function __callStatic ($name, $arguments) {
		return call_user_func_array ([self::$config, $name], $arguments);
	}
	
}