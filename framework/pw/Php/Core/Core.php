<?php
namespace pw\Php\Core;

class Core {
	
	const VERSION    = "2.0.0";
	
	static public function init () {
		// set server signatures
		$_SERVER['SERVER_SIGNATURE'] = isset($_SERVER['SERVER_SIGNATURE']) ? str_replace(
			'Server at',
			'pw.php framework/v' . self::VERSION . ' Server at', $_SERVER['SERVER_SIGNATURE']
		) : 'pw.php framework/v' . self::VERSION;
		
		Config   ::init  ();
		Cache    ::init  ();
		Route    ::init  ();
		
		
	}
	
	static public function run () {
		$info   = Route ::dispatch ();
		$status = array_shift ($info);
		
		switch ($status) {
			case \FastRoute\Dispatcher::NOT_FOUND:
				// ... 404 Not Found
				break;
			case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $routeInfo[1];
				// ... 405 Method Not Allowed
				break;
			case \FastRoute\Dispatcher::FOUND:
				list ($controller, $vars) = $info;
				$view = call_user_func_array($controller, $vars);
				// response::setView ($view);
				// response::send();
				break;
		}
			
	}
	
}

