<?php

namespace pw\Php\Core;

class Helper {
	
	/**
	 * get routes from all controllers in a dir including subdir
	 * @param	string	$dir	The dir
	 * @param	string	$basens The base namespace
	 * @return	array			The routes like
	 *   [
	 *       ['get', 'pw\Php\App\index'],
	 *       [['post', 'get'], 'submit']  // omitted pw\Php\App
	 *   ]
	 */
	static public function allRoutesFromDir ($dir, $basens = 'pw\Php\App') {
		$fsdir     = new \pw\Fs\Dir ($dir);
		if (!$fsdir->exists()) return [];
		
		$confiles  = $fsdir->files();
		$ret       = [];
		foreach ($confiles as $confile) {
			if ($confile->basename() != "controller.php")
				continue;
			$ret = array_merge ($ret, self::_routesFromControllerFile($confile, $dir, $basens));
		}
		return $ret;
	}
	
	/**
	 * Find the most recently modified time of a controller in under a dir
	 * @param	string	$dir	The dir
	 * @return	long			The max modified time
	 */
	static public function maxMTimeOfControllerFilesFromDir ($dir) {
		$mtime = 0;
		$fsdir = new \pw\Fs\Dir ($dir);
		if (!$fsdir->exists()) return $mtime;
		
		$files = $fsdir->files();
		foreach ($files as $file) {
			if ($file->basename() != "controller.php")
				continue;
				
			$mtime = max ($mtime, filemtime($file));
		}
		return $mtime;
	}
	
	/**
	 * Get the namespace (lowercased) from a controller 
	 * @param	string	$ctrlfile	The controller file
	 * @param	string	$basens The base namespace
	 * @param	string	$basedir The base dir used to infer the namespace
	 * @return	string				The namespace
	 */
	static private function _nsOfControllerFile ($ctrlfile, $basedir, $basens) {

		$ns     = '';
		$cfile  = new \pw\Fs\File ($ctrlfile);
		$basedir= new \pw\Fs\Fs ($basedir);
		
		$base   = new \pw\Fs\Fs($cfile -> dirname (true));
		while (!$base->is($basedir)) {
			$basename = $base->dirname();
			$ns = $ns . '\\' . $basename;
			$base = new \pw\Fs\Fs($base -> dirname (true));
		}
		$ret = empty($ns) ? $basens : $basens . '\\' . $ns;
		return strtolower($ret);
	}
	
	/**
	 * Get routes from a controller file
	 * @param	string	$ctrlfile	The controller file
	 * @param	string	$basens The base namespace
	 * @param	string	$basedir The base dir used to infer the namespace
	 * @return	string				The routes
	 * @see     self::allRoutesFromDir
	 */
	static private function _routesFromControllerFile ($ctrlfile, $basedir, $basens) {
		$ret = [];
		$ns  = self::_nsOfControllerFile ($ctrlfile, $basedir, $basens);
		require_once ($ctrlfile);
		$funcs = get_defined_functions ();
		
		foreach ($funcs['user'] as $func) {
			$func_lc = strtolower($func);
			if (strpos($func_lc, $ns) !== 0) continue;
			
			$func_ref = new \ReflectionFunction ($func);
			$phpdoc   = new \phpDocumentor\Reflection\DocBlock($func_ref);
			
			$defineds = $phpdoc->getTagsByName ('defined');
			if (!empty($defineds)) continue; // will add in route.php
			
			$methods  = $phpdoc->getTagsByName ('method');
			$mets     = [];
			foreach ($methods as $method) {
				$m = self::_phpdocDesc($method->getDescription ());
				if (is_array($m)) $mets = array_merge($mets, $m);
				else $mets[] = $m;
			}
			if (empty($mets)) $mets = ['GET'];
			
			$routes  = $phpdoc->getTagsByName ('route');
			$rts     = [];
			foreach ($routes as $route) {
				$r = self::_phpdocDesc($route->getDescription ());
				if (is_array($r)) $rts = array_merge($rts, $r);
				else $rts[] = $r;
			}
			if (empty($rts)) {
				// throw exception ?
			}
			foreach ($rts as $rt) {
				$ret[$rt] = [$mets, $func];
			}
		}
		return $ret;
	}

	static private function _phpdocDesc ($desc) {
		if (in_array($desc[0], ['{', '['])) {
			$desc_array = json_decode ($desc, true);
			if (json_last_error() == JSON_ERROR_NONE)
				return $desc_array;
		}
		return $desc;
	}
}