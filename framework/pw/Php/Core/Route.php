<?php
namespace pw\Php\Core;

class Route {
	
	const NOT_FOUND          = \FastRoute\Dispatcher::NOT_FOUND;
	const METHOD_NOT_ALLOWED = \FastRoute\Dispatcher::METHOD_NOT_ALLOWED;
	const FOUND              = \FastRoute\Dispatcher::FOUND;
	const CACHE_COLLECT_KEY  = __CLASS__;
	const CACHE_MTIME_KEY    = __CLASS__ . ".mtime";
	
	static private $collection;
	
	static public function init () {
		
		if (self::_useCacheOrNot()) {
			self::$collection = Cache::fetch (self::CACHE_COLLECT_KEY);
		} else {
			self::$collection = self::_collection();
			Cache::save (self::CACHE_COLLECT_KEY, self::$collection);
			Cache::save (self::CACHE_MTIME_KEY, time());
		}
	}
	
	static private function _collection () { 
		$routefile = new \pw\Fs\File (Path::ROUTE_FILE);
		
		$routes    = $routefile->exists() ? $routefile->data() : [];
		//$routes    = array_merge ($routes, self::routesFromAllControllers(), Widget::routesFromAllControllers());
		$routes    = array_merge (
			$routes,
			Helper::allRoutesFromDir(Path::APP_DIR, 'pw\Php\App'),
			Helper::allRoutesFromDir(Path::WIDGET_DIR, 'pw\Php\Widget')
		);

		$collection = \FastRoute\simpleDispatcher (function ($r) use ($routes) {
			foreach ($routes as $route => $rule) {
				$method = $rule[0];
				$ctrler = $rule[1];
				$r->addRoute ($method, $route, $ctrler);
			}
		});
		return $collection;
	}
	
	static private function _useCacheOrNot () {
		
		if (!Cache::contains(self::CACHE_COLLECT_KEY))
			return false;
		
		$routefile = new \pw\Fs\File (Path::ROUTE_FILE);
		$maxmtime = $routefile->exists() ? filemtime($routefile) : 0;
		$maxtime = max (
			Helper::maxMTimeOfControllerFilesFromDir(Path::APP_DIR),
			Helper::maxMTimeOfControllerFilesFromDir(Path::WIDGET_DIR),
			$maxmtime
		);
		
		if (Config::get("Debug")) { 
			$metime  = filemtime(__FILE__);
			$maxmtime = max ($metime, $maxmtime);
		}
		return $maxmtime <= Cache::fetch (self::CACHE_MTIME_KEY);
	}
	
	static public function dispatch () {
		$uri = rawurldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
		return self::$collection -> dispatch ($_SERVER['REQUEST_METHOD'], $uri);
	}
	
}